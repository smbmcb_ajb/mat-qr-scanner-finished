import { StatusBar } from 'expo-status-bar';
import { useState,useCallback } from 'react';
import { StyleSheet, Text, View, TextInput, Alert, Image, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import QRIcon from 'react-native-vector-icons/MaterialIcons'
import { useTheme, Button, Surface } from 'react-native-paper';
import { useFonts } from 'expo-font';
import * as SplashScreen from 'expo-splash-screen';



SplashScreen.preventAutoHideAsync();

export default function HomeScreen({navigation}) {
  const myIcon = <Icon name="wifi" size={130} color="#3C5393" />;
  const qrIcon = <QRIcon name="qr-code-scanner" size={180} color="gray" style={{marginTop:'20%'}}/>
  const [name,setName] = useState('');
  const theme = useTheme();
    
  const [fontsLoaded] = useFonts({
    'Roboto-Medium': require('../assets/fonts/Roboto-Medium.ttf'),
  });

  const onLayoutRootView = useCallback(async () => {
    if (fontsLoaded) {
      await SplashScreen.hideAsync();
    }
  }, [fontsLoaded]);

  if (!fontsLoaded) {
    return null;
  }
  return (
    <View style={{backgroundColor:theme.colors.surface,flex:1,alignItems:'center'}} onLayout={onLayoutRootView}>
        <Surface style={styles.surface}>
          <Image
            source={require('../assets/matech_logo_2.png')}
            style={{height:'40%', width:'35%',marginTop:20}}
          />
          <Text style={{fontSize:25, fontWeight:'bold',fontFamily:'Roboto-Medium', lineHeight:40, color:'#fff'}}>Warehouse Traceabitlity</Text>
        <Text style={{fontSize:25, fontWeight:'bold',fontFamily:'Roboto-Medium',marginBottom:15, color:'#fff'}}>QR Scanner</Text>
        </Surface>
        
        
        {/* {myIcon} */}
        {qrIcon}
        {/* <TextInput style={{backgroundColor:'red', width:300,height:50,borderRadius:10}} */}
        {/* onChangeText={setName} */}
        {/* /> */}
      {/* <Text>Hello Sherwin</Text> */}
      {/* <Text
      elevation={5}
        style={styles.button}
        onPress = {()=> navigation.navigate('Scanner')}
      >
        <Text>GET STARTED</Text>
      </Text> */}
      <Button 
        style={{marginTop:100}} 
        icon="qrcode-scan" 
        mode="elevated" 
        buttonColor='#3C5393' 
        contentStyle={{height:70,width:220}} 
        labelStyle={{fontSize:20, fontWeight:'bold'}} 
        textColor="white" 
        uppercase="true"
        onPress = {()=> navigation.navigate('Scanner',{'method':'mat-scan'})}
      >GET STARTED</Button>
      <StatusBar style="dark" />
    </View>
  )
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      
      // justifyContent: 'center',
    },
    button: {
      shadowColor: "black",
      shadowOpacity: 0.8,
      shadowRadius: 2,
      shadowOffset: {
        height: 1,
        width: 1
      },
      textAlign: 'center',
      alignSelf: 'center',
      // justifyContent:"center",
      // alignItems: "center",
      borderRadius: 50,
      textAlignVertical: 'center',
      backgroundColor: "#3C5393",
      padding: 10,
      width: '70%',
      height: '10%',
      fontSize: 30,
      color:'white',
      fontWeight:'bold',
      marginTop:25
    },
    surface: {
      padding: 8,
      height: '25%',
      width: "100%",
      alignItems: 'center',
      justifyContent: 'center',
      marginTop:24, 
      marginBottom:15,
      backgroundColor: "#3C5393"
    },
  });
