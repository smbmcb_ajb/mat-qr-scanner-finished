import { StatusBar } from 'expo-status-bar';
import { useState,useEffect } from 'react';
import { StyleSheet, View, Alert, Image, TouchableOpacity, ScrollView, BackHandler, Pressable, } from 'react-native';
import { SimpleAccordion } from 'react-native-simple-accordion';
import FabGroup from './FabGroup';
import ConfirmationModal from './Modal';
import { useTheme, Button, Surface, Text, TextInput, Dialog, Portal } from 'react-native-paper';
import Spinner from 'react-native-loading-spinner-overlay';


export default function QRdetails({route, navigation}) {
    const [fetchData,setFetchData] = useState(global.qrData);
    const [boxData, setBoxData] = useState([])
    const [lotNumber, setLotNumber] = useState('')
    const [modalCaller, setModalCaller] = useState(false)
    const [uploadModal, setUploadModal] = useState(false)

    const [visible, setVisible] = useState(true);

    const showDialog = () => setVisible(true);

    const hideDialog = () => setVisible(false);
    // const [inputBtn, setInpuptBtn] = useState(false)
    // const [keyboardState,setKeyboardState] = useState(true)
    // const [qrData, setQRData] = useState([route.params])
    // setFetchData(route.params.resultData[0])
  // console.log(route.params.data)
  console.log("tester")
    const testFetch = ()=>{
      
    }
    // console.log(global.boxAdditionalData)
    // console.log(global.qrData[1])

    // useEffect(()=>{
    //   // setFetchData(global.qrData)
    //   // console.log(fetchData[1])
    // },[global.qrData])

    useEffect(()=>{
      if(route.params !== undefined){
        if(route.params.modalType === "cancel"){
          setModalCaller(true)
          setUploadModal(false)
        }else if(route.params.modalType === "upload"){
          setVisible(true)
          setUploadModal(true)
          setModalCaller(false)
        }
      }
      
    },[route.params])

    useEffect(()=>{
      setLotNumber(fetchData[1].LOT_NUMBER)
      // console.log(fetchData)
    },[fetchData[1]])
    // disable hardware back button
    useEffect(() => {
        const backHandler = BackHandler.addEventListener('hardwareBackPress', () => true)
        return () => backHandler.remove()
      }, [])

   
    const view = (
        <View>
             <View style={{paddingTop:7,paddingBottom:7}}>
                 <Text variant="labelMedium" style={{alignSelf:'flex-start', color:'#3C5393'}}>INVOICE NUMBER:</Text>
                 <Text variant="titleMedium" style={{alignSelf:'flex-start'}}>{fetchData[0].INVOICE}</Text>
             </View>
             <View style={{paddingTop:7,paddingBottom:7}}>
                <Text variant="labelMedium" style={{alignSelf:'flex-start', color:'#3C5393'}}>NUMBER OF BOX:</Text>
                 <Text variant="titleMedium" style={{alignSelf:'flex-start'}}>{fetchData[0].NUMBER_OF_BOX}</Text>
             </View>
             <View style={{paddingTop:7,paddingBottom:7}}>
                <Text variant="labelMedium" style={{alignSelf:'flex-start', color:'#3C5393'}}>Quantity:</Text>
                 <Text variant="titleMedium" style={{alignSelf:'flex-start'}}>{fetchData[0].QTY}</Text>
             </View>
             <View style={{paddingTop:7,paddingBottom:7}}>
                <Text variant="labelMedium" style={{alignSelf:'flex-start', color:'#3C5393'}}>SPQ:</Text>
                 <Text variant="titleMedium" style={{alignSelf:'flex-start'}}>{fetchData[0].SPQ}</Text>
             </View>
             <View style={{paddingTop:7,paddingBottom:7}}>
                 <Text variant="labelMedium" style={{alignSelf:'flex-start', color:'#3C5393'}}>GOODS CODE:</Text>
                 <Text variant="titleMedium" style={{alignSelf:'flex-start'}}>{fetchData[0].GOODS_CODE}</Text>
             </View>
             <View style={{paddingTop:7,paddingBottom:7}}>
                <Text variant="labelMedium" style={{alignSelf:'flex-start', color:'#3C5393'}}>SUPPLIER:</Text>
                 <Text variant="titleMedium" style={{alignSelf:'flex-start'}}>{fetchData[0].SUPPLIER}</Text>
             </View>
             <View style={{paddingTop:7,paddingBottom:7}}>
                 <Text variant="labelMedium" style={{alignSelf:'flex-start', color:'#3C5393'}}>PART_NUMBER:</Text>
                 <Text variant="titleMedium" style={{alignSelf:'flex-start'}}>{fetchData[0].PART_NUMBER}</Text>
             </View>
            <View style={{paddingTop:7,paddingBottom:7}}>
                <Text variant="labelMedium" style={{alignSelf:'flex-start', color:'#3C5393'}}>DATE RECEIVE:</Text>
                 <Text variant="titleMedium" style={{alignSelf:'flex-start'}}>{fetchData[0].DATE_RECEIVE}</Text>
            </View>
            <View style={{paddingTop:7,paddingBottom:7}}>
                 <Text variant="labelMedium" style={{alignSelf:'flex-start', color:'#3C5393'}}>ASSY LINE:</Text>
                 <Text variant="titleMedium" style={{alignSelf:'flex-start'}}>{fetchData[0].ASY_LINE}</Text>
             </View>
            
        </View>
    )
   const accordionData = (
    <View style={{width:'100%'}}>  
      <SimpleAccordion 
        viewInside={view} 
        title={fetchData[0].PO}
        titleStyle={{alignSelf:'flex-start'}}

        bannerStyle={{
            shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        backgroundColor:"#fff",
        elevation: 5,
        width:'90%',
        alignSelf:'center'
        }}

        viewContainerStyle={{
            padding:8,
            paddingLeft: 20,
            // alignItems:'center',
            width: '90%',
            alignSelf:'center',
            
        }}
      />
    </View>  
)

useEffect(()=>{
  // console.log(global.boxAdditionalData.length)
  if (global.boxAdditionalData.length > 0){
    // let x = 0
    setBoxData(
      global.boxAdditionalData.map((data,index)=>{
        // x++
        return(
          
          <TextInput
            multiline={true}
            numberOfLines={1}
            key={index}
            // disabled={true}
            editable= {false}
            value={data}
            style={{width:'90%', alignSelf:'center', marginTop:10, backgroundColor:'#E5E5E5'}}
            mode="outlined"
            label={'QR/Barcode #' + (index + 1)}
            // placeholder="Input lot number"
            right={<TextInput.Icon icon="delete-forever" iconColor='red' onPress={()=>{
                // setTemporaryBoxData(global.boxAdditionalData)
                // console.log(index)
                setModalCaller(false)
                setUploadModal(false)
                setBoxData(oldValues => {
                  return oldValues.filter((_, i) => i !== index)
                })
                global.boxAdditionalData.splice(index,1)
                // temporaryBoxData.filter(index,1)
                // global.boxAdditionalData = temporaryBoxData
                
                console.log(global.boxAdditionalData)
                // console.log(temporaryBoxData)
              }} />}
            theme={{colors: {primary: '#B2B2B2', underlineColor: 'transparent'}}}
            // left={<TextInput.Icon icon="square-edit-outline" onPress={scanLotNumber}/>}
          />
        )
      })
    )
    
  }
},[global.boxAdditionalData.length])

const deleteBarcodeRow = (e)=>{
  // boxData.map((item)=>{
  //   // console.log(item.ID)
  //       if(data.ID == item.ID){
  //           // console.log(item)
  //           index = aData.findIndex(el => el.ID == item.ID)
  //           aData.splice(index, 1);
  //           // console.log(aData.indexOf(item.ID))
  //       }
  //   })
  console.log('dsfdsf')
}

const lotNumberChange = (text)=> {
  // console.log(text)
  // console.log(global.qrData[1].LOT_NUMBER)
  fetchData[1].LOT_NUMBER = text
  global.qrData[1].LOT_NUMBER = text
  setLotNumber(text); 
  // console.log(fetchData)
  setModalCaller(false)
  setUploadModal(false)
}

const lotNumberVerifier = (
  <Portal>
      <Dialog visible={visible} onDismiss={hideDialog}>
      <Dialog.Icon icon="alert" color='red' />
      <Dialog.Title style={{textAlign: 'center'}}>Missing Lot Number</Dialog.Title>
        <Dialog.Content>
            <Text style={{alignSelf:'center'}} variant="bodyMedium">Please scan code for lot number or manually input if qr/barcode is not available.</Text>
        </Dialog.Content>
        <Dialog.Actions>
          <Button labelStyle={{color:'black'}} onPress={hideDialog}>OK</Button>
        </Dialog.Actions>
      </Dialog>
    </Portal>
)
    return (
      <View style={styles.container}>
        <Surface style={styles.surface}>
          <View style={{paddingLeft: '3%', alignSelf: 'flex-start', height:'50%', width:'18%'}}>
              <Image
              source={require('../assets/matech_logo_2.png')}
              style={{height:'100%', width:'100%'}}
              />
          </View>
        </Surface>
        <Text variant='titleMedium' style={{alignSelf:'flex-start',paddingLeft:'5%', fontWeight:'bold'}}>Mat QR details</Text>
        <ScrollView style={{width:'100%'}} keyboardShouldPersistTaps="handled">
            <View style={styles.separator}/>
            {
                accordionData
            }
            <View style={styles.separator}/>
            
            <TextInput
              maxLength={50}
              multiline={true}
              numberOfLines={1}
              editable={true}
              value={lotNumber}
              style={{width:'90%', alignSelf:'center', backgroundColor:'#FFFFFF'}}
              mode="outlined"
              label="Lot Number"
              placeholder="Input lot number"
              right={<TextInput.Icon icon="qrcode-scan" iconColor='#3C5393' onPress={()=>navigation.navigate('Scanner', {'method':'lot-scan'})}/>}
              theme={{colors: {primary: '#414141', underlineColor: 'transparent'}}}
              onChangeText={lotNumberChange}
              // left={<TextInput.Icon icon="square-edit-outline" onPress={scanLotNumber}/>}
            />
            {boxData}
            
            <Button 
                style={{marginTop:30, width:'60%',alignSelf:'center', marginBottom:20}} 
                icon="qrcode-scan" 
                mode="elevated" 
                buttonColor='#3C5393' 
                contentStyle={{height:50,width:180,alignSelf:'center'}} 
                labelStyle={{fontSize:16, fontWeight:'bold'}} 
                textColor="white" 
                // uppercase="true"
                onPress = {()=> navigation.navigate('Scanner', {'method':'supplier-scan'})}
            >Supplier Code</Button>
            
            {/* <Text variant="labelSmall" style={{alignSelf:'center',color:'gray'}}>ASSY LINE:</Text>
            <Text variant="titleMedium" style={{alignSelf:'center'}}>{fetchData.ASY_LINE}</Text>
            <Text variant="labelSmall" style={{alignSelf:'center',color:'gray'}}>ASSY LINE:</Text>
            <Text variant="titleMedium" style={{alignSelf:'center'}}>{fetchData.ASY_LINE}</Text>
            <Text variant="labelSmall" style={{alignSelf:'center',color:'gray'}}>ASSY LINE:</Text>
            <Text variant="titleMedium" style={{alignSelf:'center'}}>{fetchData.ASY_LINE}</Text>
            <Text variant="labelSmall" style={{alignSelf:'center',color:'gray'}}>ASSY LINE:</Text>
            <Text variant="titleMedium" style={{alignSelf:'center'}}>{fetchData.ASY_LINE}</Text>
            <Text variant="labelSmall" style={{alignSelf:'center',color:'gray'}}>ASSY LINE:</Text>
            <Text variant="titleMedium" style={{alignSelf:'center'}}>{fetchData.ASY_LINE}</Text>
            <Text variant="labelSmall" style={{alignSelf:'center',color:'gray'}}>ASSY LINE:</Text>
            <Text variant="titleMedium" style={{alignSelf:'center'}}>{fetchData.ASY_LINE}</Text> */}
        {modalCaller ? <ConfirmationModal open={true} operation="restart your progress" content="content"/> : ""}
        {uploadModal ? (global.qrData[1].LOT_NUMBER !== "") ? <ConfirmationModal open={true} operation="upload"/>: lotNumberVerifier : ""}
        {/* {lotNumberVerifier} */}
        </ScrollView>
        {/* <SimpleAccordion viewInside={<View/>} title={"My Accordion Title"}/> */}
        <FabGroup />
        <StatusBar style="dark" />
      </View>
    )
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      
      // justifyContent: 'center',
    },
    exampleBox: {
        marginVertical: 16,
        paddingHorizontal: 16
      },
      separator: {
        height: 1,
        backgroundColor: "#000000",
        marginVertical: 16,
        width:'90%',
        alignSelf:'center'
      },
    addBtn:{
        flex:1,
        alignItems:'center',
        border:1
    },
    surface: {
        padding: 8,
        height: '10%',
        width: "100%",
        alignItems: 'flex-start',
        justifyContent:'center',
        marginTop:24, 
        marginBottom:15,
        backgroundColor:'#3C5393'
      },
  });
