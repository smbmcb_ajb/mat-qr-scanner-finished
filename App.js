import { AppRegistry } from 'react-native';
import { MD3LightTheme as DefaultTheme, Provider as PaperProvider } from 'react-native-paper';
import { name as appName } from './app.json';
import Scanner from './components/Scanner'
import HomeScreen from './components/HomeScreen'
import QRdetails from './components/QRdetails'
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';


export default function App() {
  global.secretKey = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjb21wYW55IjoiTUFUZWNoIiwiZGV2ZWxvcGVyIjoiU2hlcndpbiBCYXV0aXN0YSIsImVtcElEIjoxMDk5MH0.uel5E-cu8H-AKBzP5kqMNwjc65VaDHo90hl31CgVgjY"
  global.backend = "http://192.168.2.14:5000"
  global.qrData = [];
  global.boxAdditionalData = [];
  global.empNum = "";
  global.closScanner = true
  const Stack = createNativeStackNavigator();
  // const navigation = NavigationContainer();
  const theme = {
    ...DefaultTheme,
    colors: {
      ...DefaultTheme.colors,
      primary: 'tomato',
      secondary: 'yellow',
    },
  };
  return (
    <PaperProvider theme={theme}>
      <NavigationContainer initialRouteName="Home">
        <Stack.Navigator screenOptions={{headerShown: false}}>
          <Stack.Screen name="Home" component={HomeScreen} />
          <Stack.Screen name="Scanner" component={Scanner} />
          <Stack.Screen name="QRdetails" component={QRdetails} />
        </Stack.Navigator>
      </NavigationContainer>
    </PaperProvider>
  )

    
}

AppRegistry.registerComponent(appName, () => App);  
